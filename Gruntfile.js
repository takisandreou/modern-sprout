module.exports = function(grunt) {
 
	grunt.loadNpmTasks('grunt-contrib-sass');
	//grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	//grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-shopify');
 
	  grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		  
	    shopify: {
	      options: {
	        //You can find these in your Shopify Admin under Apps then look at the bottom for Create a private API Key
	        api_key: "be07bbed09cd7f1f15780efc485515b2",
	        password: "427b61598905bb19397128336c81f2f1",
	        url: "modernsprout.myshopify.com",
            theme : 133143875,
	        base: 'shop/' 
	      }
	    }, 
	    
	  //Run compass on css
	    compass: {
	      dist: {
	        options: {
	          sassDir: 'dev/scss',
	          cssDir: 'shop/assets',
	          specify: 'dev/scss/theme.scss',
	          outputStyle: 'compressed'
	        }
	      }
	    },
	    
	   //Sass
	    sass: {
			 dist:{
				 options:{
					style:'compressed',
					sourcemap:'none'
				 },
				 files:[{
					src:'dev/scss/theme.scss',
					dest:'shop/assets/theme.min.css'
				 }]
			 }
		 },
	   
		//Compress the JS
		uglify: {
		      options: {
		        banner: '/*!-- <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy HH:MM:ss") %> --*/\n'
		      },
		      
		      dist : {
					src : 'dev/js/*.js',
					dest : 'shop/assets/scripts.min.js'
				}
		},
	    
	    watch: {
	    	 options:{
			 spawn:false,
			 livereload:35729
		 },
		 scripts:{
			files:['dev/scss/*.scss','dev/js/*.js'],
		 	tasks:['sass','uglify']
		 },
	      shopify: {
	        files: ['shop/**'],
	        tasks: ["shopify"]
	      }
	    }
	 
	  });
	  
	  grunt.registerTask('default', ['shopify','sass', 'uglify', 'watch']);
 
};